from fastapi import FastAPI, Query, Body
from typing import Optional, Union, Set
from ipaddress import IPv4Address, IPv6Address
from pydantic import BaseModel, Field

# -- Generics ----------------------------------------------------------------

class GenericObject(BaseModel):
    name: Optional[str] = Field(None, max_length=64)
    description: Optional[str] = Field(
        None, title="Object description", 
        description="Human-readable description of the object", max_length=256
    )
    active: bool = Field(
        True, title="Object state", description="Active state of the object"
    )

class TaggedObject(GenericObject):
    tags: Set[str] = Field(
        set(), title="Object tags", description="Tags associated with the object"
    )

TagQuery = Query(
    None, title="Tag query string", 
    description="A string specifying the tag to query for"
)

app = FastAPI()

@app.get("/")
async def root():
    return {"message": "Hello World"}

# -- Unit Endpoints ----------------------------------------------------------

class NewUnit(TaggedObject):
    address: Union[IPv4Address, IPv6Address] = Field(
        ..., title="Object address", description="IPv4/v6 address of the object"
    )

class Unit(TaggedObject):
    address: Optional[Union[IPv4Address, IPv6Address]] = Field(
        None, title="Object address", description="IPv4/v6 address of the object"
    )

@app.get(
    "/units",
    response_model=Set[Unit],
    tags=["units"],
    summary="Get all units",
    response_description="A list of all units"
)
async def get_units(tags: Set[str] = TagQuery):
    return {"message": "All units"}

@app.post(
    "/units",
    response_model=Unit,
    tags=["units"],
    summary="Create a new unit",
    response_description="The created unit"
)
async def add_unit(unit: NewUnit):
    return {"message": f"Adding unit:\n{unit}"}
    
@app.get(
    "/units/{unit_name}",
    response_model=Unit,
    tags=["units"],
    summary="Get the specified unit",
    response_description="The specified unit"
)
async def get_unit(unit_name: str):
    return {"message": f"The unit {unit_name}"}

@app.patch(
    "/units/{unit_name}",
    response_model=Unit,
    tags=["units"],
    summary="Edit the specified units",
    response_description="The edited unit"
)
async def edit_unit(unit_name: str, unit: Unit):
    return {"message": f"Editing unit {unit_name}"}

@app.delete(
    "/units/{unit_name}",
    response_model=str,
    tags=["units"],
    summary="Delete the specified units",
    response_description="Name of the deleted unit",
    response_model_include="name"
)
async def delete_unit(unit_name: str):
    return {"message": f"Deleting unit {unit_name}"}

# -- Test Endpoints ----------------------------------------------------------

Test = TaggedObject

@app.get(
    "/tests",
    response_model=Set[Test],
    tags=["tests"],
    summary="Get all tests",
    response_description="A list of all tests"
)
async def get_tests(tags: Set[str] = TagQuery):
    return {"message": "All tests"}

@app.post(
    "/tests",
    response_model=Test,
    tags=["tests"],
    summary="Create a new test",
    response_description="The created test"
)
async def add_test(test: Test):
    return {"message": f"Adding test:\n{test}"}
    
@app.get(
    "/tests/{test_name}",
    response_model=Test,
    tags=["tests"],
    summary="Get the specified test",
    response_description="The specified test"
)
async def get_test(test_name: str):
    return {"message": f"The test {test_name}"}

@app.patch(
    "/tests/{test_name}",
    response_model=Test,
    tags=["tests"],
    summary="Edit the specified test",
    response_description="The edited test"
)
async def edit_test(test_name: str, test: Test):
    return {"message": f"Editing test {test_name}"}

@app.delete(
    "/tests/{test_name}",
    response_model=str,
    tags=["tests"],
    summary="Delete the specified test",
    response_description="Name of the deleted test",
    response_model_include="name"
)
async def delete_test(test_name: str):
    return {"message": f"Deleting test {test_name}"}

# -- Tag Endpoints ----------------------------------------------------

Tag = GenericObject

@app.get(
    "/tags",
    response_model=Set[Tag],
    tags=["tags"],
    summary="Get all tags",
    response_description="A list of all tags"
)
async def get_tags():
    return {"message": "All tags"}

@app.post(
    "/tags",
    response_model=Tag,
    tags=["tags"],
    summary="Create a new tag",
    response_description="The created tag"
)
async def add_tag(tag: Tag):
    return {"message": f"Adding tag:\n{tag}"}
    
@app.get(
    "/tags/{tag_name}",
    response_model=Tag,
    tags=["tags"],
    summary="Get the specified tag",
    response_description="The specified tag"
)
async def get_tag(tag_name: str):
    return {"message": f"The tag {tag_name}"}

@app.patch(
    "/tags/{tag_name}",
    response_model=Tag,
    tags=["tags"],
    summary="Edit the specified tag",
    response_description="The edited tag"
)
async def update_tag(tag_name: str, tag: Tag):
    updated_tag = tag.dict(exclude_unset=True)
    return {"message": f"Updating tag {tag_name}"}

@app.delete(
    "/tags/{tag_name}",
    response_model=str,
    tags=["tags"],
    summary="Delete the specified tag",
    response_description="Name of the deleted tag",
    response_model_include="name",
)
async def delete_tag(tag_name: str):
    return {"message": f"Deleting tag {tag_name}"}
